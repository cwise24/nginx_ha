# Overview

NGINX PLUS HA demo using CentOS 7 and Ubuntu 18.04 servers

# Windows Subsystem for Linux

## WSL Ansible Install
```
sudo apt-get install python3 python3-pip ipython3 sshpass
python3 -m pip install pip
python3 -m pip install ansible
```
**RECOMMENDED INSTALLS**
```
python3 -m pip install jmespath
```

## Changes to be made if you are using *WSL* as the local Ansible Engine

Ansible docs ansible.cfg in world readable [here](https://docs.ansible.com/ansible/latest/reference_appendices/config.html)

Windows blog post for [commands](https://devblogs.microsoft.com/commandline/chmod-chown-wsl-improvements/)


```
sudo umount /mnt/c
sudo mount C: /mnt/c -o metadata
```
## Create project ansible.cfg file
```
[defaults]
#turn off ssh key fingerprint
host_key_checking = False
#what file to use for inventory
inventory = inventory
#create log file
log_path = ansible_plays.log
#adds time stamps
#callback_whitelist = profile_tasks
#set the file for vault password
vault_password_file = .vault.key
```
# Prerequisites

Create a new directory rep_crtKey

Copy your *nginx-repo.crt* and *nginx-repo.key* here

Create your two Linux guests
Update the inventory with the correct ip addrresses

# Install components

**plus.yml** this will:
- check distro (RHEL/Debian)
- update packages (security updates)
- create 'etc/ssl/nginx' directory
- copy nginx-repo cert and key
- install ca-certificates package (RHEL)
- install ca-certificates, lsb-release, apt-transport-https, nginx_signing.key (Debian)
- install nginx-plus repo 
- install nginx-plus package
- systemd enable and start nginx-plus
- set seliunx to permissive
- Ubuntu [Unattended-Updates](https://libre-software.net/ubuntu-automatic-updates/)

**ha.yml** this will:
- check distro (RHEL/Debian)
- install nginx-ha-keepalived
- install nginx-sync

For this demo CentOS {7.9} Ubuntu {18.04} were used

# Vault key

Create ``.valut.key`` in your project repo. You'll now need to set a passphrase to use for encrypting files

```
echo -n 'supers3cret' | base64
U3VwZXJzM2NyZXQ=
```
You can add that to the file and save it

```
chmod 600 .vault.key
```

Add the file *.vault.key* to the ansible.cfg file, this will become your encryption key for all vault files.

```
[defaults]
#turn off ssh key fingerprint
host_key_checking = False
#what file to use for inventory
inventory = inventory
#create log file
log_path = ansible_plays.log
#adds time stamps
#callback_whitelist = profile_tasks
#set the file for vault password
vault_password_file = .vault.key
```

To create our encrypted sudo password file:

```
ansible-vault create sudo.yml
```
This will open a vi editor

```
---
sudo_become: "s3cr3t"
user_pass: "sUp3rS3cret!"
```

# Execution

```
ansible-playbook -i inventory plus.yml -e @sudo.yml
ansible-playbook -i inventory ha.yml -e @sudo.yml
```


# HA Initial Setup

Nginx HA [config](https://docs.nginx.com/nginx/admin-guide/high-availability/ha-keepalived/)

 As root run `nginx-ha-setup` on both instances

# Sync

[Configuration](https://docs.nginx.com/nginx/admin-guide/high-availability/configuration-sharing/)